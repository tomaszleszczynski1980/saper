from flask import Flask, render_template, request, redirect, url_for, session
from random import sample
from os import urandom
from DBconnect import test_connection
import dbtools
import auth

app = Flask(__name__)
app.secret_key = urandom(16)


def check_if_logged():

    if 'player' in session:
        player = session['player']
    else:
        player = None

    return player


@app.route('/')
def main():
    player = check_if_logged()
    error = not test_connection()

    if 'rows' in session and 'cols' in session and 'mines' in session:
        rows = session['rows']
        cols = session['cols']
        mines = session['mines']
    else:
        rows = 8
        cols = 8
        mines = 10

    return render_template('main.html', player=player, error=error,
                           rows_n=rows, cols_n=cols, mines_n=mines)


@app.route('/game')
def game():
    player = check_if_logged()
    error = not test_connection()

    rows = int(request.args.get('rows'))
    cols = int(request.args.get('cols'))
    mines = int(request.args.get('mines'))
    time = request.args.get('time')

    if time and player:
        time = int(time)
        dbtools.add_game_result(time, rows, cols, mines, player)

    try:
        mine_places = sample(range(0, rows * cols - 1), mines)
    except ValueError:
        mine_places = sample(range(0, rows * cols - 1), round(rows * cols / 5))

    session['rows'] = rows
    session['cols'] = cols
    session['mines'] = mines

    return render_template('game.html', rows=rows, cols=cols, mines=mines,
                           mine_places=mine_places, player=player, error=error)


@app.route('/results')
def results():
    player = check_if_logged()
    error = not test_connection()

    name = request.args.get('player_choice')
    level_min = request.args.get('level_min')
    level_max = request.args.get('level_max')
    sort = request.args.get('sort')
    order = request.args.get('order')

    if not level_min:
        level_min = 1

    if not level_max:
        level_max = 99

    if not sort:
        sort = 'time'

    if not order:
        order = 'asc'

    results_list = dbtools.get_results(order, name, sort, level_min, level_max)
    names = dbtools.get_names()

    return render_template('results.html', player=player, order=order,
                           results_list=results_list, names=names, error=error)


@app.route('/login', methods=['GET', 'POST'])
def login():
    player = check_if_logged()
    error = not test_connection()

    if request.method == 'GET' and not player:
        return render_template('login.html', login=True, player=player, error=error)

    elif request.method == 'GET' and player:
        session.pop('player', None)
        return redirect(request.referrer)

    else:
        name = request.form['login']
        password = request.form['password']
        user = dbtools.get_user(name)

        if user:
            hashed_password = user['password']

            if auth.password_check(password, hashed_password):
                session['player'] = name
                return redirect(url_for('main'))

            else:
                return render_template('login.html', alert="Invalid password!",
                                       login=True, player=player, error=error)
        else:
            return render_template('login.html', alert="Invalid player's name!",
                                   login=True, player=player, error=error)


@app.route('/register', methods=['GET', 'POST'])
def register():
    player = check_if_logged()
    error = not test_connection()

    if request.method == 'GET':
        return render_template('register.html', register=True, player=player, error=error)

    else:
        name = request.form['login']
        password = request.form['password']
        user = dbtools.get_user(name)

        if not user:
            hashed_password = auth.password_hash(password)
            dbtools.add_player(name, hashed_password)
            return render_template('login.html', alert=f"Player '{name}' added",
                                   login=True, player=player, name=name, error=error)
        else:
            return render_template('register.html', alert='Player already exists',
                                   register=True, player=player, error=error)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', port=5000)
