function mines_limit() {
    let cols = document.getElementById('cols');
    let rows = document.getElementById('rows');
    let mines = document.getElementById('mines');
    let level = document.getElementById('level');

    let levelValue = Math.round((mines.value / (cols.value * rows.value)) * 100);
    let max_mines = Math.round((cols.value * rows.value) * 0.99);

    mines.setAttribute('max', max_mines);
    level.setAttribute('value', levelValue);
}

mines_limit();