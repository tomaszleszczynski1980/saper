function getAlert() {
    let element = document.getElementsByClassName('dialog');
    let message = element[0].getAttribute('data-alert');
    if (message) {
        alert(message);
    }
}

getAlert();
