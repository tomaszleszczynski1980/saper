// global constants and variables
const gameField = document.getElementsByClassName('game-field');
const cols = parseInt(gameField[0].dataset.cols);
const rows = parseInt(gameField[0].dataset.rows);
const faceButton = document.getElementById('face-button');
const allFields = document.getElementsByClassName('field');
const timer = document.getElementById("time-counter");
const minesCounter = document.getElementById("mines-left-counter");
const minesNumber = minesCounter.value;
const logButton = document.getElementById('log_btn');

let timeRun = false;


function gameOver() {
    for (let field of allFields) {
        field.removeAttribute('onClick');
        field.setAttribute('oncontextmenu', 'return false;');
    }
    faceButton.style.background = "url('/static/img/loose.png')";
    gameFinish();
}


function winGame() {
    showOtherMines('flag.png');
    for (let field of allFields) {
        field.removeAttribute('onClick');
        field.setAttribute('oncontextmenu', 'return false;');
    }
    faceButton.style.background = "url('/static/img/win.png')";
    faceButton.setAttribute('onClick', 'form.submit()');
    minesCounter.setAttribute('value','0');
    gameFinish();
}


function checkIfWin() {
    let openFields = 0;

    for (let field of allFields) {
        if (field.dataset.content === 'open') {
            openFields += 1;
        }
    }

    if (openFields === (cols * rows - minesNumber)) {
        return winGame();
    }
}


function timeRunDeactivateLogBtn() {
    if (!timeRun) {
        timeRun = true;
        logButton.setAttribute('disabled', 'disabled');
        logButton.setAttribute('style', 'cursor: default; color: #dadada;');
    }
}


function setNotClickable() {
    for (let field of allFields) {
        field.style.cursor = 'default';
    }
}


function gameFinish() {
    timeRun = false;
    logButton.removeAttribute('disabled');
    logButton.removeAttribute('style');
    setNotClickable();
}


function changeTile(tile, imageName, minesNumber=0) {
    tile.setAttribute('style', `background: url('/static/img/${imageName}'); cursor: default;`);
    tile.dataset.content = 'open';
    tile.removeAttribute('onClick');
    tile.setAttribute('oncontextmenu', 'return false;');

    if (minesNumber) {
       tile.innerHTML = `<span style="font-family: 'Atari Classic Chunky'; font-size: small;
                          color: hsl(${(minesNumber * 45) - 45}, 100%, 50%);">${minesNumber}</span>`;

       /* above style attribute color is in hsl. Hue above is counted basing on mines number.
       It can be 1 - 8 mines around. Hue value is in range 0 to 360. To get 8 values from that range step should be 45.
       That is why minesNumber is multiplied by 45. Then we have to subtract 45 to get 0 (red) */
    }
}


function showOtherMines(tile) {
    for (let field of allFields) {
        let content = field.dataset.content;
        let flag = field.getAttribute('flag');

        if (content === 'mine' && !flag) {
            changeTile(field, tile);
        }
        else if (content !=='mine' && flag){
            changeTile(field, 'mine-crossed.png');
        }
    }
}


function countMinesAround(tile, tileRow, tileCol) {
    let countMines = 0;
    for (let row = tileRow - 1; row <= tileRow + 1; row++) {
        for (let col = tileCol - 1; col <= tileCol + 1; col++) {
            if (col >= 0 && col < cols && row >= 0 && row < rows) {
                let cellToCheck = document.getElementById(row * cols + col);

                if (cellToCheck.dataset.content === 'mine') {
                    countMines += 1;
                }
            }
        }
    }
    return countMines;
}


function openOtherTiles(tile, tileRow, tileCol) {
    for (let row = tileRow - 1; row <= tileRow + 1; row++) {
        for (let col = tileCol - 1; col <= tileCol + 1; col++) {
            if (col >= 0 && col < cols && row >= 0 && row < rows) {
                let cellToCheck = document.getElementById(row * cols + col);

                if (cellToCheck.dataset.content === 'empty') {
                    let minesAroundNumber = countMinesAround(cellToCheck, row, col);
                    let flag = cellToCheck.getAttribute('flag');
                    changeTile(cellToCheck, 'open-field.png', minesAroundNumber);

                    if (flag) {
                        let minesLeft =  parseInt(minesCounter.value);
                        cellToCheck.removeAttribute('flag');
                        minesCounter.setAttribute('value',`${minesLeft + 1}`);
                    }

                    if (minesAroundNumber === 0) {
                        openOtherTiles(cellToCheck, row, col);
                    }
                }
            }
        }
    }
}


// following function is game main cycle - starts on each click
function gameClick(clickedId) {
    let tile = document.getElementById(clickedId);
    let content = tile.dataset.content;

    timeRunDeactivateLogBtn();

    if (content === 'mine') {
            changeTile(tile, 'mine-red.png');
            showOtherMines('mine.png');
            gameOver();
        }
        else if (content === 'empty') {
            let tileRow = parseInt(tile.dataset.row);
            let tileCol = parseInt(tile.dataset.col);
            let minesAroundNumber = countMinesAround(tile, tileRow, tileCol);

            changeTile(tile, 'open-field.png', minesAroundNumber);

            if (minesAroundNumber === 0) {
                openOtherTiles(tile, tileRow, tileCol);
            }
            checkIfWin();
        }
}


// following function is additional game feature - adding and removing flags by right-click
function putRemoveFlag(dblClickedId) {
    let tileToFlag = document.getElementById(dblClickedId);
    let flag = tileToFlag.getAttribute('flag');
    let minesLeft =  parseInt(minesCounter.value);

    if (flag) {
        tileToFlag.setAttribute('style', `background: url('/static/img/closed-field.png');`);
        tileToFlag.removeAttribute('flag');
        tileToFlag.setAttribute('onClick', "gameClick(this.id)");
        minesCounter.setAttribute('value',`${minesLeft + 1}`);
    }
    else {
        tileToFlag.setAttribute('style', `background: url('/static/img/flag.png');`);
        tileToFlag.setAttribute('flag', 'flag');
        tileToFlag.removeAttribute('onClick');
        minesCounter.setAttribute('value',`${minesLeft - 1}`);
    }
}


//following function controls timer
function timeForward() {
    if (timeRun) {
        let actualSecond = parseInt(timer.value);
        actualSecond += 1;
        timer.setAttribute('value', actualSecond);
        if (actualSecond === 999) {
            gameOver();
        }
    }
}

setInterval(timeForward,1000);