const levelMin = document.getElementById('level_min');
const levelMax = document.getElementById('level_max');

levelMin.addEventListener('click', block_min_max);
levelMax.addEventListener('click', block_min_max);

function block_min_max () {
    levelMin.setAttribute('max', levelMax.value);
    levelMax.setAttribute('min', levelMin.value);
}