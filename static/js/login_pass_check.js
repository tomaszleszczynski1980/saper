function checkLoginPass() {
    const formRegLogin = document.getElementsByClassName('login-reg-form');
    const typeOfForm = formRegLogin[0].getAttribute('id');

    let login = document.getElementById('login').value;
    let pass = document.getElementById('password').value;
    let button = document.getElementById('submit');

    let pass2;

    if (typeOfForm === 'reg-form') {
        pass2 = document.getElementById('password2').value;
        }
    else {
        pass2 = pass;
    }

    if (login && pass.length >= 6 && pass === pass2) {
        button.removeAttribute('onclick');
        button.setAttribute('type', 'submit');
    }
    else if (login && pass && !pass2) {
        alert('Repeat password');
    }
    else if (login && pass !== pass2) {
        alert('Repeated password does not match');
    }
    else {
        alert('Enter name and password (at least 6 symbols)');
    }
}