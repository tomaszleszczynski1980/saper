from psycopg2 import sql
from DBconnect import connection_handler, connection_handler_list

# queries to database

@connection_handler
def add_player(cursor, name: str, hashed_password: str):

    query = """INSERT INTO players (name, password) VALUES (%(name)s, %(hashed_password)s);"""
    variables = {'name': name, 'hashed_password': hashed_password}

    cursor.execute(query, variables)


@connection_handler
def remove_player(cursor, player):

    query = """DELETE FROM players WHERE name = %(player)s;"""
    variables = {'player': player}

    cursor.execute(query, variables)


@connection_handler
def add_game_result(cursor, time: int, rows: int, cols: int, mines: int, player: str):

    query = f"""INSERT INTO results (player, time, rows, cols, mines)
                VALUES ('{player}', {time}, {rows}, {cols}, {mines});"""

    cursor.execute(query)


@connection_handler
def get_results(cursor, order, name=None, sort='time', level_min=1, level_max=99):

    if name:
        query = sql.SQL(f"""SELECT player, time, ((mines * 100) / (rows * cols)) AS level FROM results
                   WHERE player = %(name)s AND
                   ((mines * 100) / (rows * cols)) >= {level_min} AND ((mines * 100) / (rows * cols)) <= {level_max}
                   ORDER BY {sort} {order}""").format(sort=sql.Identifier(sort))
        variables = {'name': name}
    else:
        query = sql.SQL(f"""SELECT player, time, ((mines * 100) / (rows * cols)) AS level FROM results
                   WHERE ((mines * 100) / (rows * cols)) >= {level_min} AND ((mines * 100) / (rows * cols)) <= {level_max}
                   ORDER BY {sort} {order}""").format(sort=sql.Identifier(sort))
        variables = {}

    cursor.execute(query, variables)
    return cursor.fetchall()


@connection_handler_list
def get_names(cursor):

    query = """SELECT DISTINCT player FROM results;"""

    cursor.execute(query)
    return cursor.fetchall()


@connection_handler
def get_user(cursor, name: str):

    query = """SELECT name, password FROM players
               WHERE name = %(name)s;"""
    variables = {'name': name}

    cursor.execute(query, variables)
    return cursor.fetchone()
